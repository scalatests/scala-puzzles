package main.puzzlers.puzzler5

def sumSizes(collections: Iterable[Iterable[_]]): Int =
    collections.toSeq.map(_.size).sum

def sumSizes2(collections: Iterable[Iterable[_]]): Int =
    collections.foldLeft(0) {
        (sumOfSizes, collection) => sumOfSizes + collection.size
    }
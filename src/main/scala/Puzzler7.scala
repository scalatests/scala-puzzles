package main.puzzlers.puzzler7

import collection.mutable.Buffer

def puzzle() = {
    val accessories1 = Buffer.empty[() => Int]
    val accessories2 = Buffer.empty[() => Int]

    val data = Seq(100, 110, 120)
    var j = 0

    for(i <- 0 until data.length) {
        val currentJ = j
        accessories1 += (() => data(i))
        accessories2 += (() => data(currentJ))
        j += 1
    }

    accessories1.foreach(a1 => println(a1()))
    accessories2.foreach(a2 => println(a2()))
}
package main.puzzlers.puzzler3

trait A {
    val audiance: String
    println("Hello " + audiance)
}

class BMember(a: String = "World") extends A {
    val audiance = a
    println("I repeat: Hello " + audiance)
}

class BConstructor(val audiance: String = "World") extends A {
    println("I repeat: Hello " + audiance)
}

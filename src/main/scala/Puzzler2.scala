package main.puzzlers

class Puzzler2 {
    final val TheAnswer = 42
    def checkGuess(guess: Int) = guess match {
        case TheAnswer => "Your guess is correct"
        case _ => "Try again"
    }
}
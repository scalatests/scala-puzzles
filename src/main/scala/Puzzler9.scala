package main.puzzlers.puzzler9

def puzzle () = {
    object XY {
        object X {
            val value: Int = Y.value + 1
        }
        object Y {
            val value: Int = X.value + 1
        }
    }

   println(
        if (math.random > 0.5) XY.X.value
        else XY.Y.value
    )
}

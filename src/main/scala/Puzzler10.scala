package main.puzzlers.puzzler10

class Country(val isoCode: String, val name: String)
case class CountryCC(isoCode: String, name: String)

def puzzle () = {
    val homeOfScala = new Country("CH", "Switzerland")
    val homeOfScalaCC = CountryCC("CH", "Switzerland")

    println(homeOfScala equals new Country("CH", "Switzerland"))
    println(homeOfScalaCC equals CountryCC("CH", "Switzerland"))

    println(homeOfScala.toString)
    println(homeOfScalaCC.toString)
}

import main.puzzlers.Puzzler2

class Puzzler2Tests extends munit.FunSuite {
  test("UPSTAIRS downstairs") {
    var MONTH = 12; var DAY = 24
    var(hour, minute, second) = (12, 0, 0)
    
    assertEquals(new Puzzler2 checkGuess(21), "Try again")
    assertEquals(new Puzzler2 checkGuess(42), "Your guess is correct")
  }
}

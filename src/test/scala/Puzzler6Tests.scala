import main.puzzlers.puzzler6.applyNMutli
import main.puzzlers.puzzler6.applyNCurried
import main.puzzlers.puzzler6.nextInt
import main.puzzlers.puzzler6.nextNumber

class Puzzler6Tests extends munit.FunSuite {
  test("Arg Arrgh") {
    // 2*2 + 1 = 5
    // 5*5 + 1 = 26
    // 26*26 + 1 = 677

    assertEquals(applyNMutli(3)(2, nextInt), 677)
    assertEquals(applyNCurried(3)(2)(nextInt), 677)
    assertEquals(applyNMutli(3)(2.0, nextNumber), 677.0)
    assertEquals(applyNCurried(3)(2.0)(nextNumber), 677.0)
  }
}

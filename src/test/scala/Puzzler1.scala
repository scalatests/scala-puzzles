// For more information on writing tests, see
// https://scalameta.org/munit/docs/getting-started.html
class Puzzler1 extends munit.FunSuite {
  test("Hi there") {
    val block1 = List(1, 2).map{ i => println("Hi"); i+1 }
    println (block1)
    println("--")
    val block2 = List(1, 2).map{ println("Hi"); _ + 1 }
    println(block2)
  }
}

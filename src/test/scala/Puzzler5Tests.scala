import main.puzzlers.puzzler5.sumSizes
import main.puzzlers.puzzler5.sumSizes2


class Puzzler5Tests extends munit.FunSuite {
  test("The missing list") {
    println(sumSizes(List(Set(1, 2), List(3, 4))))
    println(sumSizes(Set(List(1, 2), Set(3, 4))))

    println(sumSizes2(List(Set(1, 2), List(3, 4))))
    println(sumSizes2(Set(List(1, 2), Set(3, 4))))
  }
}
